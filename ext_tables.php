<?php
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3_MODE') || die();

ExtensionManagementUtility::addStaticFile('typo3tuto', 'Configuration/TypoScript', 'Typo3 tuto');

