$(document).ready(function () {
	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	// ======= Accordeon Js =======

	$(".description-accordeon").css("display", "none");
	$(".title-item-accordeon").on("click", function () {
		if ($(this).siblings(".description-accordeon").css("display") == "none") {
			$(".description-accordeon").css("display", "none");
			$(this).siblings(".description-accordeon").css("display", "block");
		} else {
			$(this).siblings(".description-accordeon").css("display", "none");
		}
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	// ======= Boite à onglet Js =======

	let liNumber = 0;
	let hrefOnglet;
	liNumber = liNumber + 1;
	if (liNumber === 1) {
		$(".li-onglet").prepend(
			`<a class="nav-link active" data-bs-toggle="tab"> Onglet ${liNumber} </a>`
		);
		hrefOnglet = "#onglet-" + liNumber;
		$(".tab-pane").addClass("active");
		$(".tab-pane").attr("id", hrefOnglet);
	} else {
		$(".li-onglet").prepend(
			`<a class="nav-link" data-bs-toggle="tab"> Onglet ${liNumber} </a>`
		);
		hrefOnglet = "#onglet-" + liNumber;
		$(".tab-pane").attr("id", hrefOnglet);
		$(".tab-pane").addClass("fade");
	}

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Navbar Color */
	$("#contact-nav").css("color", "#009970");

	// Adding color to the different text section of the navbar on hover of the different page section
	$("#home-start").on({
		// When mouse enter the home section it change color for green
		mouseenter: function () {
			$("#home-nav").css("color", "#009970");
		},
		// When mouse leave the home section color is back to normal color
		mouseleave: function () {
			$("#home-nav").css("color", "");
		},
	});
	$("#about-start").on({
		mouseenter: function () {
			$("#about-nav").css("color", "#009970");
		},
		mouseleave: function () {
			$("#about-nav").css("color", "");
		},
	});
	$("#services-start").on({
		mouseenter: function () {
			$("#services-nav").css("color", "#009970");
		},
		mouseleave: function () {
			$("#services-nav").css("color", "");
		},
	});
	$("#portfolio-start").on({
		mouseenter: function () {
			$("#portfolio-nav").css("color", "#009970");
		},
		mouseleave: function () {
			$("#portfolio-nav").css("color", "");
		},
	});
	$("#team-start").on({
		mouseenter: function () {
			$("#team-nav").css("color", "#009970");
		},
		mouseleave: function () {
			$("#team-nav").css("color", "");
		},
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Responsive JQuery */
	// Header changing color according to the device width
	// Change color of the header in small and medium device
	function headerSectionResponsive() {
		// Small/Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".bg-color").addClass("bg-success").removeClass("bg-light");
			$(".nav-link").addClass("text-white ms-3");
			$(".btn-margin").addClass("ms-2").removeClass("ms-4");
		}
	}

	// Call to action changing p margin on medium and smaller device
	function callToActionSectionResponsive() {
		// Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".p-margin-call-to-action").addClass("ms-3 me-3");
		}
		// Small device
		if (window.matchMedia("(max-width: 576px)").matches) {
			$(".bg-parallax-call-to-action").css("height", "").css("height", "275px");
		}
	}

	// About section changing p margin on medium and smaller device
	function aboutSectionResponsive() {
		// Small/Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".about-margin").removeClass("ms-5");
		}
	}

	// Testimonials section changing padding on medium and smaller device
	function testimonialsSectionResponsive() {
		// Small/Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".testimonials-padding").addClass("p-0");
		}
	}

	// Team section changing padding on card on medium and smaller device
	function teamSectionResponsive() {
		// Small/Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".card-padding-team").addClass("p-0");
			$(".card-container").addClass("p-4").removeClass("p-5");
		}
	}

	// Footer section changing marging on medium device and align all the items in the center on small device
	function footerSectionResponsive() {
		// Medium device
		if (window.matchMedia("(max-width: 992px)").matches) {
			$(".footer-margin-text").addClass("ms-4");
			$(".footer-margin-fa").addClass("me-4");
		}
		// Small device
		if (window.matchMedia("(max-width: 576px)").matches) {
			$(".footer-margin-text").addClass("text-center mt-2").removeClass("ms-4");
			$(".footer-margin-fa")
				.addClass("justify-content-center mt-2 mb-3")
				.removeClass("justify-content-end me-4");
		}
	}

	headerSectionResponsive();
	callToActionSectionResponsive();
	aboutSectionResponsive();
	testimonialsSectionResponsive();
	teamSectionResponsive();
	footerSectionResponsive();

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Button Back To Top */
	// Add a button to go back to the top of the page on scroll
	// it stay fixed on the bottom right corner of the page
	// It also disapear when back to top
	$("#btn-scroll-top").hide();
	$(window).scroll(function () {
		let height = $(window).scrollTop();

		if (height < 100) {
			$("#btn-scroll-top").hide();
		}
		if (height > 100) {
			$("#btn-scroll-top").show();
		}
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ */
	/* Increment Animation */
	// Speed for the different number

	$("main-div-chiffres").ready(function () {
		let number = 0;
		$("#ul-chiffres")
			// Search the li children of the element with the id ul-chiffres and find the div in that li element
			// Then for each div in the li parent himself in the ul parent it give a variable id and after call a function to increment
			.find("li")
			.find("div")
			.each(function (i, div) {
				number++;
				$(div).prop("id", `number${number}`);
			});
		// If the div id is undefined it doesn't call the increment function
		if ($("#ul-chiffres").find("li").find("div").attr("id") === undefined) {
			return;
		} else {
			incElementNbr1("number1"); // Call this funtion with the ID-name for that element to increase the number within
			incElementNbr2("number2");
			incElementNbr3("number3");
			incElementNbr4("number4");
		}
	});

	// Speed for the different set timeout function of the different number
	let speed1 = 7;
	let speed2 = 10;
	let speed3 = 10;
	let speed4 = 200;
	let pourcent;

	/* Call this function with a string containing the ID name to
	 * the element containing the number you want to do a count animation on.*/
	function incElementNbr1(id) {
		element = document.getElementById(id);
		endNbr = Number(document.getElementById(id).innerHTML);
		// 4% pourcent of the end number
		pourcent = endNbr * 0.04;
		incNbrRec1(0, endNbr, pourcent, element);
	}

	/*A recursive function to increase the number.*/
	function incNbrRec1(i, endNbr, pourcent, element) {
		if (i <= endNbr) {
			// When i reach a certain pourcentage of the end number it change the speed so it increment more slowly
			if (i >= endNbr - pourcent) {
				speed1 = 200;
			}
			element.innerHTML = i;
			setTimeout(function () {
				//Delay a bit before calling the function again.
				incNbrRec1(i + 1, endNbr, pourcent, element);
			}, speed1);
		}
	}

	// Number 2
	function incElementNbr2(id) {
		element = document.getElementById(id);
		endNbr = Number(document.getElementById(id).innerHTML);
		// 4% pourcent of the end number
		pourcent = endNbr * 0.04;
		incNbrRec2(0, endNbr, pourcent, element);
	}

	/*A recursive function to increase the number.*/
	function incNbrRec2(i, endNbr, pourcent, element) {
		if (i <= endNbr) {
			// When i reach a certain pourcentage of the end number it change the speed so it increment more slowly
			if (i >= endNbr - pourcent) {
				speed2 = 200;
			}
			element.innerHTML = i;
			setTimeout(function () {
				//Delay a bit before calling the function again.
				incNbrRec2(i + 3, endNbr, pourcent, element);
			}, speed2);
		}
	}

	// Number 3
	function incElementNbr3(id) {
		element = document.getElementById(id);
		endNbr = Number(document.getElementById(id).innerHTML);
		// 4% pourcent of the end number
		pourcent = endNbr * 0.04;
		incNbrRec3(0, endNbr, pourcent, element);
	}

	/*A recursive function to increase the number.*/
	function incNbrRec3(i, endNbr, pourcent, element) {
		if (i <= endNbr) {
			// When i reach a certain pourcentage of the end number it change the speed so it increment more slowly
			if (i >= endNbr - pourcent) {
				speed3 = 200;
			}
			element.innerHTML = i;
			setTimeout(function () {
				//Delay a bit before calling the function again.
				incNbrRec3(i + 7, endNbr, pourcent, element);
			}, speed3);
		}
	}

	// Number 4
	function incElementNbr4(id) {
		element = document.getElementById(id);
		endNbr = Number(document.getElementById(id).innerHTML);
		// 4% pourcent of the end number
		pourcent = endNbr * 0.04;
		incNbrRec4(0, endNbr, pourcent, element);
	}

	/*A recursive function to increase the number.*/
	function incNbrRec4(i, endNbr, pourcent, element) {
		if (i <= endNbr) {
			// When i reach a certain pourcentage of the end number it change the speed so it increment more slowly
			if (i >= endNbr - pourcent) {
				speed4 = 200;
			}
			element.innerHTML = i;
			setTimeout(function () {
				//Delay a bit before calling the function again.
				incNbrRec4(i + 1, endNbr, pourcent, element);
			}, speed4);
		}
	}

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Call To Action */
	// Get the src of the image in the html and take it to a background image so  it can have the parallax effect
	$("#main-div-call-to-action").ready(function () {
		let srcImage = $(".img-parallax-call-to-action img").attr("src");
		$(".img-parallax-call-to-action").css("background-image", 'url(" ' + srcImage + ' ")');
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* OWL Carousel JQuery */
	// Owl Carousel initialisation and option
	$(".owl-carousel").owlCarousel({
		autoWidth: true,
		nav: true,
		loop: true,
		lazyLoad: true,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		margin: 10,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
			},
			600: {
				items: 1,
			},
			1000: {
				items: 2,
				dotsEach: 1,
			},
		},
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Isotope JQuery */
	// initialisation of Isotope
	$("#main-div-portfolio").ready(function () {
		let $grid = $(".grid").isotope({
			// options
			itemSelector: ".grid-item",
			layoutMode: "fitRows",
		});
		// filter items on button click
		$(".filter-button-group").on("click", "button", function () {
			let filterValue = $(this).attr("data-filter");
			$grid.isotope({ filter: filterValue });
		});

		// Add color to the filter button clicked
		// By default button All is active
		$(".btn-filter").on("click", function () {
			if (!$(this).hasClass("active")) {
				$(".btn-filter").removeClass("active");
				$(this).addClass("active");
			}
		});
	});

	/* -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_- */
	/* Player JS */
	$(".main-div-joueur").ready(function () {
		// Description JS
		// Hide the description of each player by default
		$(".field-description").css("display", "none");
		// On click of the description title it show the description under the title
		// If it's already display hit hide it
		// Change also the fa icon according of the description hide or show
		$(".complementary-description").on("click", function () {
			if ($(this).children(".field-description").css("display") == "none") {
				$(this).children(".field-description").css("display", "block");
				$(this)
					.find("i")
					.removeClass("fa-solid fa-chevron-down")
					.addClass("fa-solid fa-chevron-up");
			} else {
				$(this).children(".field-description").css("display", "none");
				$(this)
					.find("i")
					.removeClass("fa-solid fa-chevron-up")
					.addClass("fa-solid fa-chevron-down");
			}
		});
		// On hover of the player card, the fa icon get a class that animate it
		$(".player-card").on({
			mouseenter: function () {
				$(this).find("i").addClass("fa-beat");
			},
			mouseleave: function () {
				$(this).find("i").removeClass("fa-beat");
			},
		});
	});
});
