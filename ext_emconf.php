<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'typo3tuto',
    'description' => 'Tuto Typo3 Kantin',
    'category' => 'templates',
    'version' => '1.0.0',
    'state' => 'beta',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => true,
    'author' => '',
    'author_email' => 'tma@w-seils.com',
    'author_company' => 'W-Seils',
    'constraints' => [
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ],
];

